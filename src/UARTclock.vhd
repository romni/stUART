-- UARTclock.vhd
--
-- A UART baud rate clock. This entity makes the clock
-- run conform to a specific baud rate, given the processor's
-- clock frequency.
--
-- @author Rimon Oz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UARTclock is
generic (
	-- The clock frequency. default = 100Mhz
	clock_frequency: integer := 100000000;
	-- The baud rate (bauds per sec).
	baud_rate:       integer := 9600;
	-- The size of the token in bits.
	pdu_width:       integer := 8
);
port (
	clock_in:  in  std_logic;
	clock_out: out std_logic

);
end UARTclock;

architecture behavior of UARTclock is
	constant clocks_per_baud: integer := clock_frequency / (pdu_width * baud_rate); 
	signal   counter:         integer := 0;
begin
	process(clock_in)
	begin
		-- Trivial case.
		if clocks_per_baud = 1 then
			clock_out <= clock_in;
		else
			-- Otherwise count on every rising edge
			if rising_edge(clock_in) then
				-- until the threshold, and flip the sign.
				if counter = clocks_per_baud - 1 then
				  clock_out <= '1';
				  counter   <= 0;   
				else
				  clock_out <= '0';
				  counter   <= counter + 1;
				end if;
			end if;
		end if;
	end process;
end behavior;