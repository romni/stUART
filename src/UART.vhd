-- UART.vhd
--
-- A simple UART controller with one Rx and one Tx channel.
-- This controller supports 8-bit PDU's.
--
-- @author Rimon Oz
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UART is
generic (
	-- The number of data bits.
   data_width:      integer := 7;
	-- The number of parity bits.
	parity_width:    integer := 1;
	-- The parity (true for odd, false for even).
	parity:          boolean := true;
	-- The number of stop bits.
	stop_width:      integer := 1;
	-- The clock frequency.
	clock_frequency: integer := 10;
	-- The baud rate.
	baud_rate:       integer := 1
);
port (
	clock:     in  std_logic;
	-- The reset pin reset both the Rx and Tx blocks.
	reset:     in  std_logic;
	rx0:       in  std_logic;
	tx_input:  in  std_logic_vector(7 downto 0);
	tx0:       out std_logic;
	-- This vector contains the last character read.
	rx_output: out std_logic_vector(7 downto 0) := "00000000"
);
end UART;

-- The architecture of this entity simply maps the controller's
-- pins to the corresponding pins on the Rx or Tx block, respectively.
architecture behavior of UART is
	signal clock_signal: std_logic;
begin
	eCLK: entity work.UARTclock generic map(clock_frequency, baud_rate,    1 + data_width
	                                                                         + parity_width
																									 + stop_width)
										 port    map(clock,           clock_signal);
	eTX0: entity work.UARTtx    generic map(data_width,      parity_width, parity,   stop_width)
										 port    map(clock_signal,    reset,        tx_input, tx0);
	eRX0: entity work.UARTrx    generic map(data_width,      parity_width, parity,   stop_width)
										 port    map(clock_signal,    reset,        rx0,      rx_output);
end behavior;