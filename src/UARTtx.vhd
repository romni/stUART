-- UARTtx.vhd
--
-- A UART transmission entity which supports 8-bit PDU's.
--
-- @author Rimon Oz
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UARTtx is
generic (
	-- The number of data bits.
   data_width:   integer := 7;
	-- The number of parity bits.
	parity_width: integer := 1;
	-- The parity (true for odd, false for even).
	parity:       boolean := true;
	-- The number of stop bits.
	stop_width:   integer := 1
);
port (
   clock:  in  std_logic;
	reset:  in  std_logic;
	input:  in  std_logic_vector(7 downto 0);
	output: out std_logic
);
end UARTtx;

architecture behavior of UARTtx is
begin
	-- Every clock pulse or reset change.
	process(clock, reset)
		variable counter:   integer := 0;
		constant word_size: integer := 1 + data_width + parity_width;
	begin
		-- If the reset pin is active (low).
		if reset = '0' then
			counter := 0;
			output <= '1';
		-- Otherwise we're handling data.
		elsif rising_edge(clock) then
			-- Check if we're at the beginning of a frame or at the transition between
			-- frames
			if counter = 0 or counter >= word_size + stop_width then
				-- Send start bit
				output <= '0';
				-- Reset counter to take transition into account
				counter := 0;
			-- Check if we're in the middle of a PDU
			elsif counter >= 1 and counter < word_size then
				-- Send the current bit
				output <= input(counter - 1);
			-- Check if we're sending stop bits
			elsif counter >= word_size and counter < word_size + stop_width then
				-- Send a stop bit
				output <= '1';
			end if;
			counter := counter + 1;
		end if;
	end process;
end behavior;