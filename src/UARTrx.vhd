-- UARTrx.vhd
--
-- A UART receiving entity which supports 8-bit PDU's.
--
-- @author Rimon Oz
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UARTrx is
generic (
	-- The number of data bits.
   data_width:   integer := 7;
	-- The number of parity bits.
	parity_width: integer := 1;
	-- The parity (true for odd, false for even).
	parity:       boolean := true;
	-- The number of stop bits.
	stop_width:   integer := 1
);
port (
   clock:  in  std_logic;
	reset:  in  std_logic;
	input:  in  std_logic;
	output: out std_logic_vector(7 downto 0) := "00000000"
);
end UARTrx;

architecture behavior of UARTrx is
	-- The state machine.
	type RxState is (
	   idle,
		busy
	);
	
	-- The signals.
	signal state: RxState                      := idle;
	signal word:  std_logic_vector(7 downto 0) := "00000000";
	
begin
	-- Every clock pulse or reset change.
	process(clock, reset)
		variable counter:      integer := 0;
		variable stop_counter: integer := 0;
	begin
		-- If the reset pin is active (low).
		if reset = '0' then
			word    <= "00000000";
			output  <= "00000000";
			state <= idle;
		-- Otherwise we're handling data.
		elsif rising_edge(clock) then
			case state is
				when idle =>
					-- If we've received a start bit
					if input = '0' then
						-- clear the data line and the counter
						word    <= "00000000";
						counter := 0;
						-- then switch to receiving.
						state <= busy;
					end if;
				when busy =>
					-- If there's 8 data bits
					if counter = data_width + parity_width then
						-- count the stop bits
						stop_counter := stop_counter + 1;
						-- until there's enough stop bits.
						if stop_counter = stop_width then
							-- Reset the counters.
							counter      := 0;
							stop_counter := 0;
							-- Push the data to the output and switch the state.
							output <= word;
							state  <= idle;
						end if;
					-- If we're counting data bits, then
					elsif stop_counter = 0 then
						-- save the bit and increment the counter.
						word(counter) <= input;
						counter := counter + 1;
					end if;
			end case;
		end if;
	end process;
end behavior;